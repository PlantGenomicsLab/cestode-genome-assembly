#!/bin/bash
#SBATCH --job-name=flye_assembly_cestode_200130_HRMinION_JW278b_rmv_contam_input
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 36
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mem=200G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%flye.out
#SBATCH -e %x_%flye.err

module load flye/2.5

flye --nano-raw /projects/EBP/Wegrzyn/cestode/200130_HRMinION_Multi/Demultiplexing/PASS/cestode_200130_HRMinION_JW278b_rmv_contam_reads.fastq --genome-size 250m --out-dir flye_assembly_cestode_200130_HRMinION_JW278b_rmv_contam_input --threads 36

