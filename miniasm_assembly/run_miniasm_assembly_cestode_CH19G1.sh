#!/bin/bash
#SBATCH --job-name=miniasm_assembly_cestode_CH19G1_rmv_contam_input
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=250G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load minimap2/2.17
module load miniasm/0.3
module load racon/0.5.0

minimap2 -x ava-ont -t 32 /projects/EBP/Wegrzyn/cestode/200130_HRMinION_Multi/Demultiplexing/PASS/cestode_200130_HRMinION_CH19G1_rmv_contam_reads.fastq /projects/EBP/Wegrzyn/cestode/200130_HRMinION_Multi/Demultiplexing/PASS/cestode_200130_HRMinION_CH19G1_rmv_contam_reads.fastq | gzip -1 > cestode_CH19G1_reads_pass_rmv_contam.paf.gz
miniasm -R -f /projects/EBP/Wegrzyn/cestode/200130_HRMinION_Multi/Demultiplexing/PASS/cestode_200130_HRMinION_CH19G1_rmv_contam_reads.fastq cestode_CH19G1_reads_pass_rmv_contam.paf.gz > cestode_CH19G1_miniasm_assembly_rmv_contam_input.gfa
