#!/bin/bash
#SBATCH --job-name=shasta_assembly_cestode_200130_HRMinION_CH19G1_minlen_500
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=150G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load shasta/0.4.0

shasta --input cestode_200130_HRMinION_CH19G1_rmv_contam_reads.fasta --assemblyDirectory shastarun_cestode_200130_HRMinION_CH19G1_minlen_500 --Reads.minReadLength 500 --memoryMode anonymous --memoryBacking 4K --threads 16

