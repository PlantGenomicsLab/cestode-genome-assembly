# Cestode Genome Assembly
Project Spreadsheet: https://docs.google.com/spreadsheets/d/1tWJ5lK05PYNNJloNVw3eu4d2GXzmUbGDZD9aF-CVqis/edit?usp=sharing  

Creating an assembly of the Cestode genome using Oxford Nanopore long-read and Illumina short-read data.  

## 1). DNA Sequencing
2 Cestode samples were sequenced using Oxford Nanopore GridION and Illumina sequencing.

## **2) Refine Input Reads.**  

## **Centrifuge Classification of Combined Long-read Data:**  
**Full Centrifuge Classification Report:** https://docs.google.com/spreadsheets/d/1tWJ5lK05PYNNJloNVw3eu4d2GXzmUbGDZD9aF-CVqis/edit#gid=1567829313  
Script is located in **centrifuge** directory.  

Cestode combined input long-reads were analyzed with centrifuge sequence classification program, results are on project spreadsheet.  
Script information in **centrifuge_cestode_minion_reads.sh**  
min-hit-len parameter is raised to 50 to correct for false-positive classification hits in error-prone minion data.  
*     Input: Raw long-reads, and Bacteria, Aarchaea, Viruses, and Human genomic sequence index database 
      Output: Centrifuge sequence classification report  
      (Genomic sequence index available for download on official Centrifuge website: https://ccb.jhu.edu/software/centrifuge/manual.shtml) 

**Primary Contaminants in Long-read Data:**  
- synthetic construct
    - NumReads: 1674
- Gilman virus
    - NumReads: 31
- Vibrio harveyi
    - NumReads: 5

#### Nanoplot Analysis of Long-read Data Post-Centrifuge QC:
Nanoplot report html files for both long-read samples located in **nanoplot** directory.  

## 3). Genome Assembly

## Shasta Assembly
Shasta genome assembler was run with long-read input, quast results are on spreadsheet  
Scripts located in **shasta_assembly** directory.  
**Note:** BUSCO Analysis of CH19G1 sample assembly and QUAST results of each sample indicates that Shasta did not assemble correctly (See below)  
*     Input: Basecalled Oxford Nanopore raw long-read sequences in fasta format 
      Output: Draft long-read genome assembly  

**FastqToFasta.py** converts basecalled PromethION fastq sequencing reads to fasta format.  

**run_shasta_assembly_cestode_CH19G1_rmv_contam_default_minlen.sh** runs the Shasta genome assembly program with basecalled fasta reads as input and minimum accepted input read length of 10,000 base pairs.
- Key Parameters in **run_shasta_assembly_cestode_CH19G1_rmv_contam_default_minlen.sh**:  
    - --memoryMode anonymous --memoryBacking 4K  
        - Optimal Shasta assembly memory allocation settings that can run without sudo priveleges.  

**run_shasta_assembly_cestode_CH19G1_rmv_contam_minlen_500.sh** runs the Shasta genome assembly program with basecalled fasta reads as input and minimum accepted input read length of 500 base pairs.  
- Key Parameters in **run_shasta_assembly_cestode_CH19G1_rmv_contam_minlen_500.sh**:  
    - --memoryMode anonymous --memoryBacking 4K  
        - Optimal Shasta assembly memory allocation settings that can run without sudo priveleges.  
    - --Reads.minReadLength 500  
        - Discards reads less than 500bp before assembly.  

**run_shasta_assembly_cestode_JW278b_rmv_contam_minlen_500.sh** runs the Shasta genome assembly program with basecalled fasta reads as input and minimum accepted input read length of 500 base pairs.  
- Key Parameters in **run_shasta_assembly_cestode_JW278b_rmv_contam_minlen_500.sh**:  
    - --memoryMode anonymous --memoryBacking 4K  
        - Optimal Shasta assembly memory allocation settings that can run without sudo priveleges.  
    - --Reads.minReadLength 500  
        - Discards reads less than 500bp before assembly.  

**Cestode CH19G1 Shasta Assembly (default minlen 10,000):**  
*     /projects/EBP/Wegrzyn/cestode/shasta_assembly/shastarun_cestode_200130_HRMinION_CH19G1_default_minlen/Assembly.fasta
      
      Genome Size: 216682
      # of Contigs: 64
      N50: 6268

**Cestode CH19G1 Shasta Assembly (minlen 500):**  
*     /projects/EBP/Wegrzyn/cestode/shasta_assembly/shastarun_cestode_200130_HRMinION_CH19G1_minlen_500/Assembly.fasta
      
      Genome Size: 49742265
      # of Contigs: 5218
      N50: 17241
      BUSCO:
        Eukaryota: C:0.0%[S:0.0%,D:0.0%],F:1.2%,M:98.8%,n:255
        Metazoa: C:0.3%[S:0.3%,D:0.0%],F:1.6%,M:98.1%,n:954 

**Cestode JW278b Shasta Assembly (minlen 500):**  
*     /projects/EBP/Wegrzyn/cestode/shasta_assembly/shastarun_cestode_200130_HRMinION_JW278b_minlen_500/Assembly.fasta
      
      Genome Size: 49119
      # of Contigs: 16
      N50: 3437

**Full Shasta QUAST Stats:** https://docs.google.com/spreadsheets/d/1tWJ5lK05PYNNJloNVw3eu4d2GXzmUbGDZD9aF-CVqis/edit#gid=0  
**Full Shasta BUSCO Stats:** https://docs.google.com/spreadsheets/d/1tWJ5lK05PYNNJloNVw3eu4d2GXzmUbGDZD9aF-CVqis/edit#gid=431467486  

## **Flye Assembly**  
Script is located in **flye assembly** directory.  

*     Input: Basecalled Oxford Nanopore raw long-read sequences in fastq format 
      Output: Draft long-read genome assembly  
**flye_assembly_cestode_200130_HRMinION_CH19G1_rmv_contam_input.sh** runs the Flye genome assembly program with basecalled fastq sequencing reads as input.  
- Key Parameters in **flye_assembly_cestode_200130_HRMinION_CH19G1_rmv_contam_input.sh**:
    - --genome-size 250m
        - Rough genome size estimate.  

**flye_assembly_cestode_200130_HRMinION_JW278b_rmv_contam_input.sh** runs the Flye genome assembly program with basecalled fastq sequencing reads as input.  
- Key Parameters in **flye_assembly_cestode_200130_HRMinION_JW278b_rmv_contam_input.sh**:
    - --genome-size 250m
        - Rough genome size estimate.  

**Cestode CH19G1 Flye Genome Assembly:**  
*     /projects/EBP/Wegrzyn/cestode/flye_assembly/flye_assembly_cestode_200130_HRMinION_CH19G1_rmv_contam_input/assembly.fasta
      
      Genome Size: 218062693
      # of Contigs: 4330
      N50: 155934
      BUSCO:
        Eukaryota: C:27.1%[S:26.7%,D:0.4%],F:25.1%,M:47.8%,n:255
        Metazoa: C:24.4%[S:24.0%,D:0.4%],F:15.3%,M:60.3%,n:954

**Cestode JW278b Flye Genome Assembly:**  
*     /projects/EBP/Wegrzyn/cestode/flye_assembly/flye_assembly_cestode_200130_HRMinION_JW278b_rmv_contam_input/assembly.fasta
      
      Genome Size: 1825711
      # of Contigs: 299
      N50: 14016

**Full Flye QUAST Stats:** https://docs.google.com/spreadsheets/d/1tWJ5lK05PYNNJloNVw3eu4d2GXzmUbGDZD9aF-CVqis/edit#gid=1320152890  
**Full Flye BUSCO Stats:** https://docs.google.com/spreadsheets/d/1tWJ5lK05PYNNJloNVw3eu4d2GXzmUbGDZD9aF-CVqis/edit#gid=633100069  

## **Masurca Short-read Assembly**  
Scripts are located in **masurca_illumina_only_assembly** directory.  

*     Input: Basecalled short-read Illumina sequencing reads in fastq format   
      Output: Draft Illumina short-read genome assembly
**config_illumina_only_CH19G1.txt** contains information on input reads and parameters for Masurca genome assembly run.  
**run_masurca_cestode_assembly_CH19G1_illumina_only.sh** runs Masurca genome assembly program with short-read Illumina sequencing reads as input.

- Key Parameters in **config_illumina_only_CH19G1.txt**:
    - PE= aa 350 50 R1 R2
        - 'aa' is a prefix for identifying short-read input files, each read set has a unique prefix.
        - 350 is the mean read length of each Illumina read.
        - 50 is the standard deviation of the Illumina reads.
    - JF_SIZE = 2200000000
        - JF_SIZE = rough genome size estimate x 10

**Cestode CH19G1 Masurca Short-Read Genome Assembly:**  
*     /projects/EBP/Wegrzyn/cestode/masurca_illumina_assembly/masurca_assembly_CH19G1/CA/final.genome.scf.fasta
      
      Genome Size: 227317123
      # of Contigs: 15866
      N50: 31778
      BUSCO:
        Eukaryota: C:60.0%[S:59.6%,D:0.4%],F:16.5%,M:23.5%,n:255
        Metazoa: C:46.7%[S:46.2%,D:0.5%],F:12.2%,M:41.1%,n:954

**Full Masurca Assembly QUAST Stats:** https://docs.google.com/spreadsheets/d/1tWJ5lK05PYNNJloNVw3eu4d2GXzmUbGDZD9aF-CVqis/edit#gid=900648583  
**Full Masurca Assembly BUSCO Stats:** https://docs.google.com/spreadsheets/d/1tWJ5lK05PYNNJloNVw3eu4d2GXzmUbGDZD9aF-CVqis/edit#gid=774683006  

## **Masurca Hybrid Assembly**
Scripts are located in **masurca_hybrid_assembly** directory.  

*     Input: Basecalled short-read Illumina sequencing reads and long-read Nanopore sequencing reads in fastq format  
      Output: Draft masurca hybrid genome assembly  
**config_hybrid_assembly_CH19G1.txt** contains information on input reads and parameters for Masurca genome assembly run.  
**run_hybrid_masurca_cestode_assembly.sh** runs Masurca genome assembly program with short-read Illumina and long-read Nanopore sequencing reads as input.  

- Key Parameters in **config_hybrid_assembly_CH19G1.txt**:
    - PE= aa 350 50 R1 R2
        - 'aa' is a prefix for identifying short-read input files, each read set has a unique prefix.
        - 350 is the mean read length of each Illumina read.
        - 50 is the standard deviation of the Illumina reads.
    - JF_SIZE = 2200000000
        - JF_SIZE = rough genome size estimate x 10

**Cestode CH19G1 Masurca Hybrid Assembly:**  
*     /projects/EBP/Wegrzyn/cestode/masurca_hybrid_assembly/masurca_hybrid_assembly_CH19G1/CA.mr.41.15.15.0.02/final.genome.scf.fasta
      
      Genome Size: 235318567
      # of Contigs: 1890
      N50: 348754
      BUSCO:
        Eukaryota: IN PROGRESS
        Metazoa: IN PROGRESS

**Full Masurca Assembly QUAST Stats:** https://docs.google.com/spreadsheets/d/1tWJ5lK05PYNNJloNVw3eu4d2GXzmUbGDZD9aF-CVqis/edit#gid=900648583  
**Full Masurca Assembly BUSCO Stats:** https://docs.google.com/spreadsheets/d/1tWJ5lK05PYNNJloNVw3eu4d2GXzmUbGDZD9aF-CVqis/edit#gid=774683006  

## **Miniasm Assembly**
Script is located in **miniasm_assembly** directory.

*     Input: Basecalled long-read Nanopore sequencing reads in fastq format
      Output: Draft miniasm genome assembly
**run_miniasm_assembly_cestode_CH19G1.sh** runs miniasm genome assembly program with long-read Nanopore sequencing reads as input.  

- Key Parameters in **run_miniasm_assembly_cestode_CH19G1.sh**:
    - minimap2 -x ava-ont
        - Set to Nanopore read overlap
    - miniasm -R
        - -R parameter discards clearly contaminated read from input

**Cestode CH19G1 Miniasm Assembly:**  
*     /projects/EBP/Wegrzyn/cestode/miniasm_assembly/miniasm_assembly_CH19G1/cestode_CH19G1_miniasm_assembly_rmv_contam_input.fasta
      
      Genome Size: 15284653
      # of Contigs: 915
      N50: 19617
      BUSCO:
        Eukaryota: ERROR: Augustus did not recognize any genes matching the dataset eukaryota_odb10 in the input file.
        Metazoa: C:0.0%[S:0.0%,D:0.0%],F:0.0%,M:100.0%,n:954

**Full Miniasm Assembly QUAST Stats:** https://docs.google.com/spreadsheets/d/1tWJ5lK05PYNNJloNVw3eu4d2GXzmUbGDZD9aF-CVqis/edit#gid=1710571659  
**Full Miniasm Assembly BUSCO Stats:** https://docs.google.com/spreadsheets/d/1tWJ5lK05PYNNJloNVw3eu4d2GXzmUbGDZD9aF-CVqis/edit#gid=1855124186  
