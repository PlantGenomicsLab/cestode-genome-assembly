#!/bin/bash
#SBATCH --job-name=centrifuge_cestode_200130_HRMinION_Multi
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=250G
#SBATCH --mail-user=andrew.starovoitov@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load centrifuge/1.0.4-beta

centrifuge -x p+h+v --report-file centrifuge_cestode_200130_HRMinION_Multi_report.tsv --quiet --min-hitlen 50 -q /projects/EBP/Wegrzyn/cestode/200130_HRMinION_Multi/cestode_200130_HRMinION_Multi_reads.fastq


